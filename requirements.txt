selenium==4.7.0
pydantic==1.10.2
pymongo==4.3.3
flask==2.2.2
flask-pymongo==2.3.0