from web.extensions import mongo
from pymongo import DESCENDING


def list_news(topic=None, date=None, limit=10, skip=0):
    kwargs = {"limit": limit, "skip": skip, "filter": {}}
    if topic:
        kwargs["filter"]["topic"] = topic
    if date:
        kwargs["filter"]["date"] = date
    items = []
    for item in mongo.db.get_collection("news").find(**kwargs).sort("date", DESCENDING):
        items.append(item)
    return items


def count_news(topic=None):
    kwargs = {"filter": {}}
    if topic:
        kwargs["filter"]["topic"] = topic
    count = mongo.db.news.count_documents(**kwargs)
    return count

def save_news(news:dict):
    _news = mongo.db.news.find_one({'link': news['link']})
    if _news:
        return _news
    mongo.db.news.insert_one(news)
    return news