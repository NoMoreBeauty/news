from typing import List

from bson import ObjectId

from web.extensions import mongo
from web.models import Topic


def show_topic(topic_id) -> Topic:
    query = {"_id": ObjectId(topic_id)}
    topic = mongo.db.get_collection("topics").find_one(query)
    return Topic(**topic)


def list_topics(query:dict = None) -> List[Topic]:
    kwargs = {"filter": {'deleted': False}}
    if query:
        kwargs['filter'].update(query)
    return [Topic(**topic) for topic in mongo.db.topics.find(**kwargs)]


def update_topic(topic_id, data):
    mongo.db.topics.update_one({"_id": ObjectId(topic_id)}, {"$set": data})


def create_topic(name) -> Topic:
    data = {"name": name, "enable": True, "deleted": False}
    mongo.db.topics.insert_one(data)
    return Topic(**data)
