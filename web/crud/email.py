from web.extensions import mongo
from web.models import Email
from typing import List
from pymongo import ASCENDING
from bson import ObjectId


def create_receiver(name, email) -> Email:
    data = {"name": name, "email": email, "deleted": False, "enable": True}
    mongo.db.email.insert_one(data)
    return Email(**data)


def list_receivers(skip: int = 0, limit: int = 10) -> List[Email]:
    cursor = mongo.db.email.find().sort("name", ASCENDING)
    if limit:
        cursor = cursor.limit(limit)
    if skip:
        cursor = cursor.skip(skip)

    return [Email(**email) for email in cursor]


def update_receiver(uuid: str, data: dict):
    query = {"_id": ObjectId(uuid)}
    mongo.db.email.update_one(query, {"$set": data})


def delete_one_receiver(uuid: str):
    query = {"_id": ObjectId(uuid)}
    mongo.db.email.delete_one(query)
