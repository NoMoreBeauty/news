from web.extensions import mongo
def list_sites():
    return [site for site in mongo.db['sites'].find()]