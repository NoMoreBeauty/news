from web.extensions import mongo
from web.models import Keyword, Topic
from typing import List
from bson import ObjectId


def count_keywords(topic: str = None) -> int:
    kwargs = {"filter": {}}
    if topic:
        kwargs["filter"]["topic"] = topic
    return mongo.db.keywords.count_documents(**kwargs)


def list_keywords(query: dict = None, skip: int = 0, limit: int = 10) -> List[Keyword]:
    kwargs = {"filter": {"deleted": False}}
    if limit:
        kwargs.update(dict(limit=limit, skip=skip))
    if query:
        kwargs["filter"].update(query)
    
    return [Keyword(**data) for data in mongo.db.keywords.find(**kwargs)]


def create_keyword(topic: Topic, name: str):
    data = {"enable": True, "name": name, "topic": topic.name, "deleted": False}
    mongo.db.keywords.insert_one(data)
    return Keyword(**data)


def update_keyword(keyword_id, data):
    query = {"_id": ObjectId(keyword_id)}
    mongo.db.keywords.update_one(query, {"$set": data})
