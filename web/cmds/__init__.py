from web.cmds.mail_sender.cmd import send_mail
from web.cmds.news_crawler.cmd import fetch_command
from flask import Flask


def init_app(app: Flask):
    fetch_command()
    send_mail(None)
