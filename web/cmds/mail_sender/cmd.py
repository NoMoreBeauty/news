from datetime import datetime, timedelta

import click

from web.cmds.mail_sender.main import MailGenerator, MailSender
from web.crud.email import list_receivers



# @click.command("mail", help="send news report with email")
# @click.option("--date", "-d", help="报告时间")
def send_mail(date):
    if not date:
        date = (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")
    receivers = [email.email for email in list_receivers(limit=0)]
    message = MailGenerator().generate(date)
    MailSender(receivers).send(f"{date}舆情报告", message)
