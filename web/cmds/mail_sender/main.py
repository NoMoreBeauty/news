import smtplib
import sys
from copy import deepcopy
from email.mime.text import MIMEText
from email.header import Header
from pymongo import MongoClient
import pymongo
import configparser


class Mail_Distributor:

    # number of topics
    total_topics = 6

    cp = configparser.ConfigParser()

    def __init__(self):
        self.cp.read("configure.cfg", encoding="utf-8")
        self.smtp_server = self.cp.get("mail", "smtp_server")

        self.user = self.cp.get("mail", "user")
        self.password = self.cp.get("mail", "password")

        self.sender = self.cp.get("mail", "sender")

        self.receiver = self.cp.get("mail", "receiver").split(" ")
        self.html_text = ""

        self.host = self.cp.get("mongo", "host")
        self.port = int(self.cp.get("mongo", "port"))
        self.db = self.cp.get("mongo", "db")

        self.topics = self.cp.get("classes", "key_words").split(",")
        self.topics_dict = dict(enumerate(self.cp.get("classes", "topics").split(",")))

        self.client = MongoClient(
            self.cp.get("mongo", "host"),
            int(self.cp.get("mongo", "port")),
            username=self.cp.get("mongo", "username"),
            password=self.cp.get("mongo", "password"),
        )

    def send_email(self):

        subject = "Email test"

        msg = MIMEText(self.html_text, "html", "utf-8")
        msg["Subject"] = Header(subject, "utf-8")
        msg["From"] = "test"

        smtp = smtplib.SMTP()
        try:

            smtp.connect(self.smtp_server)

            smtp.set_debuglevel(1)  # 显示调试信息
            smtp.login(self.user, self.password)
            smtp.sendmail(self.sender, self.receiver, msg.as_string())
        except Exception as e:
            print(e)
        finally:
            smtp.quit()

    def read_data_from_db(self):
        try:
            db = self.client[self.db]
            self.r = [[] for _ in range(self.total_topics)]
            for index in range(self.total_topics):
                for x in db.news.find({"topic": self.topics_dict[index]}):
                    # TODO 修改flag值
                    self.r[index].append(x)
            # self.r = [self.r[0],[],[],[],[],[]]
            print(self.r)
        except Exception as e:
            print(e)

    def splicing_html(self):
        self.get_keywords()
        self.read_data_from_db()
        self.construct_mail()

    def construct_catalogue(self) -> str:
        try:
            f = open("resources/catalogue.html", encoding="utf-8", errors="ignore")
            x = f.read()
            f.close()

            rst = ""
            for i in range(self.total_topics):
                if len(self.r[i]) > 0:
                    t = x.replace("mc_catalogue_title", self.topics[i])
                    t = t.replace("mc_catalogue_number", str(len(self.r[i])))
                    t = t.replace("mc_catalogue_search", "search" + str(i + 1))
                    rst += t
            return rst
        except IOError as e:
            print(e)

    def construct_subject(self) -> str:
        try:
            f = open("resources/subject.html", encoding="utf-8", errors="ignore")
            x = f.read()
            f.close()

            rst = ""

            for i in range(self.total_topics):
                if len(self.r[i]) > 0:
                    t_title = self.construct_subject_title(i)
                    t_main = self.construct_subject_main(i)
                    t = x.replace("mc_subject_title", t_title)
                    t = t.replace("mc_subject_main", t_main)
                    rst += t
            return rst
        except IOError as e:
            print(e)

    def construct_subject_title(self, index) -> str:
        try:
            f = open("resources/subject_title.html", encoding="utf-8", errors="ignore")
            x = f.read()
            f.close()

            rst = x.replace("mc_subject_title_title", self.topics[index])
            rst = rst.replace("mc_subject_title_number", str(len(self.r[index])))
            rst = rst.replace("mc_subject_search", "search" + str(index + 1))
            return rst
        except IOError as e:
            print(e)

    def construct_subject_main(self, index) -> str:
        try:
            f = open("resources/subject_main.html", encoding="utf-8", errors="ignore")
            y = f.read()
            f.close()

            rst = ""

            for i in self.r[index]:
                x = deepcopy(y)
                x = x.replace(
                    "mc_news_host_page", "https://news.163.com/"
                )  # i['source']
                x = x.replace("mc_news_host_name", i["source"])
                x = x.replace("mc_news_time", i["date"])  # i['time']
                x = x.replace("mc_news_title", i["title"])
                x = x.replace("mc_news_text", i["content"])
                x = x.replace("mc_news_url", i["link"])
                self.em_keywords(x, index)
                rst += x
            return rst
        except IOError as e:
            print(e)

    def em_keywords(self, content: str, index: int):
        for k in self.key_words[index]:
            if k in content:
                content = content.replace(k, "<em>" + k + "</em>")

    def construct_mail(self):
        try:
            f = open("resources/frame.html", encoding="utf-8", errors="ignore")
            x = f.read()
            f.close()

            catalogue = self.construct_catalogue()
            subject = self.construct_subject()

            x = x.replace("mc_catalogue", catalogue)
            x = x.replace("mc_subject", subject)

            self.html_text = x
            f = open("temp.html", encoding="utf-8", mode="w")
            f.write(x)
            f.close()

        except IOError as e:
            print(e)

    def get_keywords(self):
        try:
            db = self.client[self.db]
            self.key_words = [[] for _ in range(self.total_topics)]
            for index in range(self.total_topics):
                for x in db.keywords.find({"topic": self.topics_dict[index]}):
                    # TODO 修改flag值
                    self.key_words[index].append(x["name"])
            print(self.key_words[0])
        except Exception as e:
            print(e)
        pass

    def run(self):
        self.read_data_from_db()
        self.get_keywords()
        self.construct_mail()
        self.send_email()


import os
from web.settings import SMTP_HOSTNAME, SMTP_PASSWORD, SMTP_USERNAME
from jinja2 import Environment, FileSystemLoader, select_autoescape
from web.crud.topic import list_topics
from web.crud.news import list_news
from web.crud.email import list_receivers


class MailSender:
    def __init__(self, receivers=None):
        self.smtp = smtplib.SMTP()
        self.receivers = receivers or []

    def connect_smtp_server(self):
        self.smtp.connect(SMTP_HOSTNAME)
        self.smtp.login(SMTP_USERNAME, SMTP_PASSWORD)

    def generate_message(self, title, content):
        subject = title
        msg = MIMEText(content, "html", "utf-8")
        msg["Subject"] = Header(subject, "utf-8")
        msg["From"] = "1187322738@qq.com"
        return msg

    def send(self, title, content):
        self.connect_smtp_server()
        try:
            msg = self.generate_message(title, content)
            self.smtp.sendmail("1187322738@qq.com", self.receivers, msg.as_string())
        finally:
            self.smtp.quit()
        return


class MailGenerator:
    def __init__(self):
        self.__environment = None

    @property
    def environment(self) -> Environment:
        if self.__environment:
            return self.__environment
        template_dir = os.path.join(os.path.dirname(__file__), "resources")
        __loader = FileSystemLoader(template_dir)
        self.__environment = Environment(
            autoescape=select_autoescape(["html", "htm", "xml"]),
            loader=__loader,
        )
        return self.__environment

    def load_news_statistic(self, date: str):
        context = {"news": {}, "statistic": {}}
        topics = list_topics({"deleted": False, "enable": True})
        for topic in topics:
            news = list_news(topic=topic.name)
            context["news"][topic.name] = news
            context["statistic"][topic.name] = {"counter": len(news), "id": topic.uuid}
        return context

    def generate(self, date: str):
        contexts = self.load_news_statistic(date)
        template = self.environment.get_template("frame.html")
        return template.render(contexts)


if __name__ == "__main__":
    date = "2023-04-25"
    message = MailGenerator().generate(date)
    MailSender().send(f"{date}舆情报告", message)

    # Mail_Distributor().run()
    # Mail_Distributor().splicing_html()
    # Mail_Distributor().read_data_from_db()
