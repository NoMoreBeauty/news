import os
import time
import traceback
import uuid
from datetime import datetime, timedelta
from urllib.parse import urlencode

import click
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ChromeOptions, Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait

from web.crud.keywords import list_keywords
from web.crud.news import save_news
from web.crud.topic import list_topics
from web.models import Keyword
from web.settings import SELENIUM_HUB_HOST, SELENIUM_HUB_PORT


def get_avalible_keywords(topic: str = None):
    query = {"enable": True}
    if topic:
        query["name"] = topic
    topics = [topic.name for topic in list_topics(query)]
    
    query = {"topic": {"$in": topics}, "enable": True, "deleted": False}
    
    for keyword in list_keywords(query, limit=0):
        yield keyword
    # keywords_list = [Keyword(uuid='2133',name='沐曦',topic='沐曦')]
    # for keyword in keywords_list:
    #     yield keyword


def get_next_page_button(driver: Remote):
    try:
        element = driver.find_element(By.CLASS_NAME, "page-inner")
    except NoSuchElementException:
        return None
    items = element.find_elements(By.TAG_NAME, "a")
    if "下一页" in items[-1].text:
        return items[-1]
    return None


def parse_news_content(item):
    try:
        return item.find_element(By.XPATH, "div/div/div[2]/span[2]").text
    except NoSuchElementException:
        return item.find_element(By.XPATH, "div/div/div/span[2]").text


def parse_news_source(item):
    try:
        return item.find_element(By.XPATH, "div/div/div[2]/div/a")
    except NoSuchElementException:
        return item.find_element(By.XPATH, "div/div/div/div/a")


def parse_search_content(driver: Remote, keyword: Keyword, date_str: str):
    element = WebDriverWait(driver, 3).until(
        lambda x: x.find_element(By.ID, "content_left")
    )
    for item in element.find_elements(By.CLASS_NAME, "result-op"):
        try:
            title = item.find_element(By.XPATH, "div/h3/a").text
            print("title")
            content = parse_news_content(item)
            source = parse_news_source(item)
            source_text = source.text
            source_link = source.get_attribute("href")
            data = dict(
                title=title,
                content=content,
                source=source_text,
                link=source_link,
                topic=keyword.topic,
                date=date_str,
            )
            save_news(data)
        except NoSuchElementException:
            print(f"PARSE ERROR: {keyword}, {item.text}")
            screenshot_dir = os.path.join(
                os.path.dirname(__file__),
                "../screenshots",
            )
            if not os.path.isdir(screenshot_dir):
                os.mkdir(screenshot_dir)

            screenshot_file = os.path.join(
                screenshot_dir,
                f"{uuid.uuid4()}.png",
            )
            driver.save_screenshot(screenshot_file)
            print(traceback.format_exc())


def search_and_parse_keyword(driver: Remote, keyword: Keyword, start, end):
    base_url = "https://www.baidu.com/s?"
    params = {
        "ie": "utf-8",
        "medium": 0,
        "rtt": 1,
        "bsst": 1,
        "rsv_dl": "news_t_sk",
        "cl": 2,
        "wd": '"' + keyword.name + '"',
        "tn": "news",
        "oq": None,
        "rsv_btype": "t",
        "f": 8,
        "gpc": f"stf={start},{end}|stftype=1",
    }
    url = base_url + urlencode(params)
    print(keyword, url)
    driver.get(url)
    date_str = time.strftime("%Y-%m-%d", time.localtime(end))
    while True:
        parse_search_content(driver, keyword, date_str)
        button = get_next_page_button(driver)
        if not button:
            break
        for __ in range(3):
            element = driver.find_element(By.TAG_NAME, "body")
            element.send_keys(Keys.PAGE_DOWN)
            time.sleep(2)
        button.click()


# @click.command("fetch", help="fetch news with topics")
def fetch_command():
    options = ChromeOptions()
    executor = f"http://{SELENIUM_HUB_HOST}:{SELENIUM_HUB_PORT}"

    today = datetime.today()
    base_time = datetime(year=today.year, month=today.month, day=today.day)
    if time.timezone == 0:
        base_time = base_time - timedelta(hours=8)
    start = base_time.timestamp() + 0.001
    end = (base_time + timedelta(hours=24)).timestamp() - 0.003
    
    for keyword in get_avalible_keywords():
        driver = Remote(command_executor=executor, options=options)
        try:
            search_and_parse_keyword(driver, keyword, start, end)
            time.sleep(15)
        finally:
            driver.quit()
