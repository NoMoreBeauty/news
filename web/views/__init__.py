from . import news, keywords, topics, sites, email
from flask import Flask


def init_app(app: Flask):
    news.init_app(app)
    keywords.init_app(app)
    topics.init_app(app)
    sites.init_app(app)
    email.init_app(app)

