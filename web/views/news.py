from flask import Blueprint, Flask, render_template, request
from flask.views import MethodView

from web.crud.news import count_news, list_news
from web.crud.topic import list_topics, show_topic

class NewsList(MethodView):
    def get(self):
        topics = list_topics()
        topic_id = request.args.get("topic")
        skip = int(request.args.get("skip", 0))
        if topic_id:
            topic = show_topic(topic_id)
            news = list_news(topic=topic.name, skip=skip)
            count = count_news(topic.name)
        else:
            news = list_news(skip=skip)
            count = count_news()
         
            
        context = {
            "items": news,
            "topics": topics,
            "topic_id": topic_id,
            "skip": skip,
            "count": count,
        }
        return render_template("news/list.html", **context)


def init_app(app: Flask):
    bp = Blueprint("news", __name__) #蓝图就是把很多的视图函数分开来管理
    bp.add_url_rule("/", view_func=NewsList.as_view("news-list")) # 访问时通过访问   xxxxxx/news/访问NewsList.as_view("news-list")视图函数
    app.register_blueprint(bp)
