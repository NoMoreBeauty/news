from flask import Blueprint, Flask, render_template, request
from flask.views import MethodView

from web.crud.sites import list_sites


class SiteList(MethodView):
    def get(self):
        sites =  list_sites()
        context = {'sites': sites}
        return render_template('./sites.html', **context)


def init_app(app:Flask):
    bp = Blueprint('sites', __name__, url_prefix="/config/sites")
    bp.add_url_rule('/', view_func=SiteList.as_view('site-list'))
    app.register_blueprint(bp)