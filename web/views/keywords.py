from flask import Blueprint, Flask, redirect, request, url_for
from flask.views import MethodView

from web.crud.topic import show_topic
from web.crud.keywords import create_keyword, update_keyword


class KeywordCreateView(MethodView):
    def post(self):
        topic_id = request.form.get("topic")
        topic = show_topic(topic_id)
        name = request.form.get("keyword")
        create_keyword(topic, name)
        return redirect(url_for("topic.topic-detail", topic_id=topic_id))


class KeywordUpdateView(MethodView):
    def post(self):
        topic_id = request.form.get("topic")
        keyword_id = request.form.get("keyword_id")
        keyword = request.form.get("keyword")
        enable = request.form.get("enable") != None
        update_keyword(keyword_id, {"name": keyword, "enable": enable})
        return redirect(url_for("topic.topic-detail", topic_id=topic_id))


class KeywordDeleteView(MethodView):
    def post(self):
        topic_id = request.form.get("topic")
        keyword_id = request.form.get("keyword_id")
        update_keyword(keyword_id, {"deleted": True})
        return redirect(url_for("topic.topic-detail", topic_id=topic_id))


def init_app(app: Flask):
    bp = Blueprint("keywords", __name__, url_prefix="/config/keywords")
    bp.add_url_rule("/create", view_func=KeywordCreateView.as_view("keyword-create"))
    bp.add_url_rule("/update", view_func=KeywordUpdateView.as_view("keyword-update"))
    bp.add_url_rule("/delete", view_func=KeywordDeleteView.as_view("keyword-delete"))
    app.register_blueprint(bp)
