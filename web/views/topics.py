from flask import Blueprint, Flask, render_template, request, redirect, url_for
from flask.views import MethodView

from web.crud.keywords import count_keywords, list_keywords
from web.crud.topic import list_topics, show_topic, update_topic, create_topic


class TopicListView(MethodView):
    def get(self):
        topics = list_topics()
        for topic in topics:
            topic.keywords = count_keywords(topic.name)
        context = {"topics": topics}
        return render_template("topic/list.html", **context)

    def post(self):
        name = request.form.get("topic")
        topic = create_topic(name)
        return redirect(url_for("topic.topic-detail", topic_id=topic.uuid))


class TopicDetailView(MethodView):
    def get(self, topic_id):
        skip = int(request.args.get("skip", 0))
        topic = show_topic(topic_id)
        keywords = list_keywords(dict(topic=topic.name), skip=skip)
        topic.keywords = count_keywords(topic.name)
        context = {
            "topic": topic,
            "keywords": keywords,
            "skip": skip,
            "count": topic.keywords,
        }
        return render_template("topic/detail.html", **context)

    def post(self, topic_id):
        name = request.form.get("topic")
        enable = request.form.get('enable') != None
        update_topic(topic_id, {"name": name, "enable": enable})
        return redirect(url_for("topic.topic-detail", topic_id=topic_id))


class TopicDeleteView(MethodView):
    def post(self, topic_id):
        update_topic(topic_id=topic_id, data={'deleted':True})
        return redirect(url_for("topic.topic-list"))


def init_app(app: Flask):
    bp = Blueprint("topic", __name__, url_prefix="/config/topics")
    bp.add_url_rule(
        "/",
        view_func=TopicListView.as_view("topic-list"),
    )
    bp.add_url_rule(
        "/<topic_id>",
        view_func=TopicDetailView.as_view("topic-detail"),
    )
    bp.add_url_rule(
        "/<topic_id>/delete",
        view_func=TopicDeleteView.as_view("topic-delete"),
    )
    app.register_blueprint(bp)
