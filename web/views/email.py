from flask import Blueprint, Flask, redirect, render_template, request, url_for
from flask.views import MethodView

from web.crud.email import (
    create_receiver,
    delete_one_receiver,
    list_receivers,
    update_receiver,
)


class EmailListView(MethodView):
    def get(self):
        emails = list_receivers()
        print(emails)
        return render_template("email/list.html", emails=emails)


class EmailCreateView(MethodView):
    def post(self):
        create_receiver(request.form.get("name"), request.form.get("email"))
        return redirect(url_for("email.email-list"))


class EmailUpdateView(MethodView):
    def post(self):
        uuid = request.form.get("uuid")
        name = request.form.get("name")
        email = request.form.get("email")
        data = {"name": name, "email": email}
        update_receiver(uuid, data)
        return redirect(url_for("email.email-list"))


class EmailDeleteView(MethodView):
    def post(self):
        uuid = request.form.get("uuid")
        delete_one_receiver(uuid)
        return redirect(url_for("email.email-list"))


def init_app(app: Flask):
    bp = Blueprint("email", __name__, url_prefix="/config")
    bp.add_url_rule("/emails", view_func=EmailListView.as_view("email-list"))
    bp.add_url_rule("/emails/create", view_func=EmailCreateView.as_view("email-create"))
    bp.add_url_rule("/emails/update", view_func=EmailUpdateView.as_view("email-update"))
    bp.add_url_rule("/emails/delete", view_func=EmailDeleteView.as_view("email-delete"))
    app.register_blueprint(bp)
