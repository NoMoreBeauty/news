from flask import Flask

from web import extensions
from web import views
from web import cmds


app = Flask(__name__)
app.config.from_object("web.settings")
extensions.init_app(app)   #数据库连接
views.init_app(app) #
cmds.init_app(app)

@app.context_processor  #钩子函数，在浏览器请求前执行 返回一个字典，其中的键值对可以在templates所有的模板中使用
def menu_list():
    topics = extensions.mongo.db.get_collection("topics")
    context = {"topics": []}
    for topic in topics.find({"enable": True}):
        context["topics"].append({"id": str(topic["_id"]), "name": topic["name"]})
    return context
