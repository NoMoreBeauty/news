from pydantic import BaseModel, root_validator


class POCSModel(BaseModel):
    @root_validator(pre=True)
    def model_process(cls, values):
        if "_id" in values:
            values["uuid"] = str(values.get("_id"))
        return values


class Topic(POCSModel):
    uuid: str
    name: str
    enable: bool = True
    deleted: bool = False
    keywords: int = 0


class Keyword(POCSModel):
    uuid: str
    name: str
    topic: str
    enable: bool = True
    deleted: bool = False


class Email(POCSModel):
    uuid: str
    name: str
    email: str
    enable: bool = True
    deleted: bool = False