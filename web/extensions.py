from flask_pymongo import PyMongo

mongo = PyMongo()
from flask import Flask
from pymongo import MongoClient
from pymongo.database import Database


class MongoDB:
    def __init__(self, app: Flask = None):
        self.cx: MongoClient = None
        self.db: Database = None
        if app:
            self.init_app(app)

    def init_app(self, app: Flask):
        host = app.config.get("MONGO_HOST", "43.159.35.31")
        port = app.config.get("MONGO_PORT", 27017)
        username = app.config.get("MONGO_USERNAME")
        password = app.config.get("MONGO_PASSWORD")
        kwargs = {"host": host, "port": int(port)}
        if username and password:
            kwargs.update({"username": username, "password": password})
        self.cx = MongoClient(**kwargs)

        db_name = app.config.get("MONGO_DBNAME")
        if db_name:
            self.db = self.cx.get_database(db_name)


mongo = MongoDB()

def init_app(app: Flask):
    mongo.init_app(app)
