FROM python:3.10-alpine

ADD web /var/app/web
ADD requirements.txt /var/app
ENV PYTHONPATH="/var/app/"
ENV FLASK_APP="main"
EXPOSE 5000
EXPOSE 22
WORKDIR /var/app/web

RUN pip install -r /var/app/requirements.txt

CMD ["flask","run","--host","0.0.0.0"]
